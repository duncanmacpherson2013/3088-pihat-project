# 3088 PiHAT Project

# The PiHAT will attach to a Raspberry Pi and us the input and output signals of the Pi. The HAT will have an audio input which will be processed by a series of subsystems e.g. EQ and volume controls and indicators and will be outputted on an analog audio output which will be accessible to the Pi as well. 

Subsystem 1 - Input Signal  
Inputs  
Negative rail  
Positive rail  
GND  
Input signal  
Volume attenuation knob   
LF attenuation knob  
HF attenuation knob  

Outputs  
Aggregated signal output (Adjusted HF and LF)  

Specifications  
Amplify signal to a maximum of 0.8 Vpp  
Attenuate Low frequency (0 to 1k) Hz  
Attenuate High frequency (1k to 20k) Hz  
Knob controls attenuation for each range - 100% is 0dB and 0% is -30dB  
Attenuation could be done using potentiometers  

Subsystem 2 - LEDs   
Inputs  
Power Supply (PiHAT 5V)  
GND  
Input signal  
Output signal   
Volume attenuation knob  

Outputs  
Input power indicator  
Output power indicator  
Volume intensity indicator   

Specifications  
Draw power from the PiHAT  
If audio jack connected to input LED switches on  
If audio jack connected to output LED switches on  
A set of LEDs indicate the volume intensity of the signal.   

Subsystem 3 - Switching Power Regulator  
Inputs  
Power Supply (PiHAT 5V)   
GND  

Outputs  
Negative rail Power Supply  
Positive rail Power Supply  

Specifications  
Draw power from PiHAT  
Modify Voltage to required value for Op-Amp  
Power rails for Op-Amp  
Provide positive voltage for positive rail  
Provide negative voltage for negative rail  


